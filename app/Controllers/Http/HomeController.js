//app/Controllers/Http/HomeController.js
'use strict'

class HomeController {

  index({ view }) {
    return view.render('index')
  }

  switchLang({ response, params, antl }) {
    const lang = params.lang
    const locales = antl.availableLocales()
    if (locales.indexOf(lang) > -1 ) {
      response.cookie('lang', lang, { path: '/' })
    }
    response.redirect('back')
  }

}

module.exports = HomeController