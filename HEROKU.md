# Internacionalizar una aplicación con AdonisJS

Resultado del tutorial: [#007 - Internacionalizar una aplicación con AdonisJS](http://www.victorvr.com/tutorial/internacionalizar-una-aplicacion-con-adonisjs)

Demo: [https://adonisjs-antl.herokuapp.com](https://adonisjs-antl.herokuapp.com)

![App](http://www.victorvr.com/img/posts/Post-07.png)

# Instalación de la aplicación en Heroku

## Requerimientos

Esta aplicación asume que tienes lo siguiente instalado en tu computadora:

`node >= 8.0` o mayor.

```bash
node --version
```

`npm >= 5.0` o mayor.

```bash
npm --version
```

## Instalación de Heroku CLI

1.- Primero necesitamos instalar `Heroku CLI`.

```bash
npm install -g heroku
```

2.- Verificamos la instalación `heroku >= 7.0` o mayor.

```bash
heroku --version
```

## Instalación de la aplicación en Heroku

1.- Ingresar al directorio de la aplicación `adonisjs-antl`:

```bash
cd adonisjs-antl
```

2.- Iniciar sesión en Heroku con los datos de nuestra cuenta.

```bash
heroku login -i
```

3.- Crea la aplicación `adonisjs-antl` en Heroku.

```bash
heroku create adonisjs-antl
```

4.- Agregar el repositorio remoto de Heroku a nuestro repositorio local.

```bash
heroku git:remote -a adonisjs-antl
```

5.- Agregar las siguientes variables de configuración.

```bash
heroku config:set HOST=::
heroku config:set APP_URL=https://adonisjs-antl.herokuapp.com
```

6.- Visualizar las variables de configuración.
```bash
heroku config
```

7.- Implementar nuestra aplicación en Heroku.

```bash
git push heroku master
```

8.- Abrir la aplicación en el navegador.
```bash
heroku open
```