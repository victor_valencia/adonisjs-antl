//app/Middleware/Locale.js
'use strict'

class Locale {

  async handle ({ view, request, antl }, next) {
    const lang = request.cookie('lang') || 'en'
    if (lang) {
      antl.switchLocale(lang)
    }
    view.share({
      lang: lang,
      flags: {
        de: 'de',
        en: 'us',
        es: 'mx',
        fr: 'fr',
        pt: 'pt'
      },
      locales: antl.availableLocales()
    })
    await next()
  }

}

module.exports = Locale