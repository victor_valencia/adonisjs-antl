# Internacionalizar una aplicación con AdonisJS

Resultado del tutorial: [#007 - Internacionalizar una aplicación con AdonisJS](http://www.victorvr.com/tutorial/internacionalizar-una-aplicacion-con-adonisjs)

Demo: [https://adonisjs-antl.herokuapp.com](https://adonisjs-antl.herokuapp.com)

![App](/public/app.gif)

## Requerimientos

Esta aplicación asume que tienes lo siguiente instalado en tu computadora:

`node >= 8.0` o mayor.

```bash
node --version
```

`npm >= 5.0` o mayor.

```bash
npm --version
```

## Instalación de Adonis CLI

Primero necesitamos instalar `Adonis CLI`:

```bash
npm i -g adonisjs-cli
```

## Instalación de Dependencias via NPM

Ahora instalaremos las dependencias de nuestra aplicación:

```bash
npm install
```

## Iniciar el servidor
Ejecutamos la aplicación:

```bash
adonis serve --dev
```

## Abrir la aplicación
Y por último, abrimos [http://localhost:3333](http://localhost:3333) en el navegador.